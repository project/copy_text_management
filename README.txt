CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * How to use
 * Maintainers


INTRODUCTION
------------

A module that stores simple configuration variables which can be configured using Drupal's admin UI by the site administrator. 
This comes useful when a site displays these copy text contents in the front end which are changed based on business needs (example: Information text, maintenance text, banner text, notification text, error/success message)

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


HOW TO USE
-------------

1.After installation login as admininstrator.
2.Go to the Configuration -> System and click on the Copy text.
3.Default 2 options are available Success & Error messages
4.In the custom module if you want copy text value then use copy_text_manager services.
   Syntax $message = \Drupal::service('copy_text_management.copy_text_manager')->get('COPY_TEXT_MACHINE_NAME);
   e.g. $message = \Drupal::service('copy_text_management.copy_text_manager')->get('registration);


MAINTAINERS
-----------

Current maintainers:
 * Deepak R Mali - https://www.drupal.org/u/deepakrmali

