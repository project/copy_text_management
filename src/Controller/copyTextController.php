<?php

namespace Drupal\copy_text_management\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\copy_text_management\journeyInterface;

/**
 * An example controller.
 */
class copyTextController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function add(journeyInterface $journey) {

    $copy_text = $this->entityTypeManager()->getStorage('copy_text')->create([
      'parent' => $journey->id(),
    ]);

    $form = $this->entityFormBuilder()->getForm($copy_text);

    return $form;
  }

  /**
   * Returns a render-able array for a test page.
   */
  public function edit(journeyInterface $copy_text) {

    $form = $this->entityFormBuilder()->getForm($copy_text);

    return $form;
  }

}
