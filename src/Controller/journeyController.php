<?php

namespace Drupal\copy_text_management\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\copy_text_management\journeyInterface;

/**
 * An example controller.
 */
class journeyController extends ControllerBase {

  /**
   * Returns a render-able array for a test page.
   */
  public function content(journeyInterface $journey) {

    $build = $this->entityTypeManager()->getListBuilder('copy_text')->renderList($journey);
    return $build;
  }

}
