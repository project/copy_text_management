<?php

namespace Drupal\copy_text_management\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\copy_text_management\journeyInterface;

/**
 * Defines the Maintence entity.
 *
 * @ConfigEntityType(
 *   id = "copy_text",
 *   label = @Translation("Copy text"),
 *   handlers = {
 *     "list_builder" = "Drupal\copy_text_management\Controller\copyTextBuilder",
 *        "form" = {
 *       "default" = "Drupal\copy_text_management\Form\copyTextForm",
 *         "edit" = "Drupal\copy_text_management\Form\copyTextForm",
 *       "delete" = "Drupal\copy_text_management\Form\copyTextDeleteForm"
 *     }
 *   },
 *   config_prefix = "copy_text",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "parent",
 *     "description"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/journey/copytext/{journey}/{copy_text}/edit",
 *     "delete-form" = "/admin/config/system/journey/copytext/{copy_text}/delete",
 *   }
 * )
 */
class copyText extends ConfigEntityBase implements journeyInterface {

  /**
   * The Maintence ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Maintence label.
   *
   * @var string
   */
  public $label;

}
