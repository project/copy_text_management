<?php

namespace Drupal\copy_text_management\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\copy_text_management\journeyInterface;

/**
 * Defines the Maintence entity.
 *
 * @ConfigEntityType(
 *   id = "journey",
 *   label = @Translation("Journey"),
 *   handlers = {
 *     "list_builder" = "Drupal\copy_text_management\Controller\journeyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\copy_text_management\Form\journeyForm",
 *       "edit" = "Drupal\copy_text_management\Form\journeyForm",
 *       "delete" = "Drupal\copy_text_management\Form\journeyDeleteForm",
 *     }
 *   },
 *   config_prefix = "journey",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   config_export = {
 *     "id",
 *     "label"
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/system/journey/{journey}",
 *     "delete-form" = "/admin/config/system/journey/{journey}/delete",
 *     "manage-form" = "/admin/config/system/journey/copytext/{journey}",
 *   }
 * )
 */
class journey extends ConfigEntityBase implements journeyInterface {

  /**
   * The Maintence ID.
   *
   * @var string
   */
  public $id;

  /**
   * The Maintence label.
   *
   * @var string
   */
  public $label;

}
