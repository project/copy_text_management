<?php

namespace Drupal\copy_text_management\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form handler for the Maintence add and edit forms.
 */
class copyTextForm extends EntityForm {
  private $journey;

  /**
   * Constructs an MaintenceForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {

    $form = parent::form($form, $form_state);
    $entity = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#description' => $this->t("Label for the Example."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$entity->isNew(),
    ];

    $form['description'] = [
      '#title' => t('Copy text'),
      '#type' => 'textarea',
      '#default_value' => $entity->get('description'),
      '#description' => t('Enter the copy text'),
    ];

    // You will need additional form elements for your custom properties.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;

    $status = $entity->save();

    // Set the submitted configuration setting.
    if ($status) {
      $t_args = ['%label' => $entity->label()];
      drupal_set_message(t('Saved the %label copy text.', $t_args));
    }
    else {
      $t_args = ['%label' => $entity->label()];
      drupal_set_message(t('The %label copy text was not saved.', $t_args));

    }
    $parameters['journey'] = $entity->parent;
    $form_state->setRedirect('entity.journey.manage_form', $parameters);
  }

  /**
   * Helper function to check whether an Maintence configuration entity exists.
   */
  public function exist($id) {
    $entity = $this->entityTypeManager->getStorage('copy_text')->getQuery()
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }

}
