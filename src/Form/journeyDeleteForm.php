<?php

namespace Drupal\copy_text_management\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Builds the form to delete an Maintence.
 */
class journeyDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.journey.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $copy_texts = \Drupal::entityTypeManager()->getStorage('copy_text')->loadByProperties(['parent' => $this->entity->id()]);
    foreach ($copy_texts as $copy_text) {
      $copy_text->delete();
    }
    $t_args = ['%label' => $this->entity->label()];
    $this->entity->delete();
    drupal_set_message(t('The %label journey has been deleted.', $t_args));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
