<?php

namespace Drupal\copy_text_management;

/**
 * Reject when running from the command line or when HTTP method is not safe.
 *
 * The policy denies caching if the request was initiated from the command line
 * interface (drush) or the request method is neither GET nor HEAD (see RFC
 * 2616, section 9.1.1 - Safe Methods).
 */
class copyTextManager {

  /**
   * {@inheritdoc}
   */
  public function get($copy_text_id, $default = '') {

    $copy_text = \Drupal::entityTypeManager()->getStorage('copy_text')->load($copy_text_id);

    $message = $default;
    if ($copy_text) {
      $message = $copy_text->get('description');
    }
    return $message;
  }

}
