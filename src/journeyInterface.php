<?php

namespace Drupal\copy_text_management;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an maintence entity.
 */
interface journeyInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
